from distutils.core import setup

setup(
    name='moex_connector',
    version='0.1',
    description='MOEX API connector',
    author='meznick',
    packages=['moex']
)
